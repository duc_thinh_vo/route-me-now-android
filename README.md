# RouteMeNow

> RouteMeNow is an Android written in Kotlin which allows the users to easily find the itineraries in Uusimaa area. 

----

![](demo-1.png)
![](demo-2.png)
![](demo-3.png)
![](demo-4.png)

## Requirements
* Android Studio 3
* Google Maps API Key ([Create one](https://console.cloud.google.com/apis) if you do not have yet)
* Google Maps API Key should be placed inside `google_maps_key` in `google_maps_api.xml` file.

----
## Dependencies
* Apollo Android(GraphQL client)
* Retrofit and Gson
* Okhttp
* Room Database

----
## App's features
* Search public transport in Uusimaa area
* Cache searched locations
* Cache bookmarked locations

----
## Tech
* Well structured code. The app includes components: Activities, Broadcasts, Services
* MVVM pattern with data binding and LiveData
* Architecture Components
* The app consumes GraphQL API from HSL for routing service
* The app consumes Restful Geocoding service from HSL(to utilize Retrofit)

----
### What is missing? Todos
1. Unit tests(does not follow TDD)
2. Allow user to change the map type in Settings section
3. There are some pieces of code do not follow the MVVM pattern.
