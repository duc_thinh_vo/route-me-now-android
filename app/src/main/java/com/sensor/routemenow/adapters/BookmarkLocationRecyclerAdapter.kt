package com.sensor.routemenow.adapters

import android.app.Activity
import android.app.Application
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sensor.routemenow.R
import com.sensor.routemenow.database.entities.BookmarkLocation
import com.sensor.routemenow.databinding.RecyclerItemBookmarkLocationBinding
import com.sensor.routemenow.extensions.inflate
import com.sensor.routemenow.viewmodels.BookmarkLocationItemViewModel

class BookmarkLocationRecyclerAdapter(val context: Context, val bookmarks: List<BookmarkLocation>?): RecyclerView.Adapter<BookmarkLocationRecyclerAdapter.BookmarkLocationViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): BookmarkLocationViewHolder {
        val view = parent.inflate(R.layout.recycler_item_bookmark_location)
        return BookmarkLocationViewHolder(view)
    }

    override fun getItemCount() = bookmarks?.size ?: 0

    override fun onBindViewHolder(viewHolder: BookmarkLocationViewHolder, position: Int) {
        bookmarks?.get(position)?.let {
            val viewModel = BookmarkLocationItemViewModel(it)
            viewHolder.setViewModel(viewModel)
        }
    }


    inner class BookmarkLocationViewHolder(view: View): RecyclerView.ViewHolder(view) {
        private val binding = DataBindingUtil.bind<RecyclerItemBookmarkLocationBinding>(view)

        fun setViewModel(viewModel: BookmarkLocationItemViewModel) {
            binding?.viewModel = viewModel
        }
    }
}