package com.sensor.routemenow.adapters

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.sensor.routemenow.R
import com.sensor.routemenow.extensions.inflate
import com.sensor.routemenow.others.LegModeViewHolder

class LegsModeRecyclerAdapter(val legs: List<ItineraryPlanQuery.Leg>): RecyclerView.Adapter<LegModeViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, position: Int): LegModeViewHolder {
        val view = parent.inflate(R.layout.recycler_item_leg_mode)
        return LegModeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return legs.size
    }

    override fun onBindViewHolder(viewHolder: LegModeViewHolder, position: Int) {
        val leg = legs[position]
        viewHolder.leg = leg
    }
}