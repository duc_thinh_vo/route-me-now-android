package com.sensor.routemenow.adapters

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sensor.routemenow.R
import com.sensor.routemenow.databinding.RecyclerItemLocationBookmarkPickTypeBinding
import com.sensor.routemenow.extensions.inflate
import com.sensor.routemenow.models.LocationBookmarkType
import com.sensor.routemenow.viewmodels.LocationBookmarkPickListItemViewModel

class LocationBookmarkPickListAdapter(val context: Context): RecyclerView.Adapter<LocationBookmarkPickListAdapter.LocationBookmarkPickListViewHolder>() {

    var listener: LocationBookmarkPickListAdapterListener? = null

    private val types: List<LocationBookmarkType> = listOf<LocationBookmarkType>(
            LocationBookmarkType.WORK,
            LocationBookmarkType.FAVORITE,
            LocationBookmarkType.SCHOOL,
            LocationBookmarkType.SHOPPING,
            LocationBookmarkType.RESTAURANT,
            LocationBookmarkType.GYM,
            LocationBookmarkType.HOME)

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): LocationBookmarkPickListViewHolder {
        val view = parent.inflate(R.layout.recycler_item_location_bookmark_pick_type)
        return LocationBookmarkPickListViewHolder(view)
    }

    override fun getItemCount(): Int = types.size

    override fun onBindViewHolder(holderView: LocationBookmarkPickListViewHolder, position: Int) {
        val bookmarkType = types[position]
        holderView.setViewModel(LocationBookmarkPickListItemViewModel(bookmarkType))
        holderView.itemView.setOnClickListener {
            val type = types[position]
            listener?.onSelectedLocationBookmarkType(type)
        }
    }

    inner class LocationBookmarkPickListViewHolder(val view: View): RecyclerView.ViewHolder(view) {
        private val binding = DataBindingUtil.bind<RecyclerItemLocationBookmarkPickTypeBinding>(itemView)

        fun setViewModel(viewModel: LocationBookmarkPickListItemViewModel) {
            binding?.viewModel = viewModel
        }
    }

    interface LocationBookmarkPickListAdapterListener {
        fun onSelectedLocationBookmarkType(type: LocationBookmarkType)
    }
}