package com.sensor.routemenow.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.sensor.routemenow.R
import com.sensor.routemenow.extensions.inflate
import com.sensor.routemenow.models.LocationSearchRow
import com.sensor.routemenow.models.LocationSearchRowType
import com.sensor.routemenow.others.LocationSearchViewHolder

class LocationSearchRecyclerAdapter(val context: Context, val locationList: List<LocationSearchRow>): RecyclerView.Adapter<LocationSearchViewHolder>() {

    var listener: LocationSearchRecyclerAdapterListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): LocationSearchViewHolder {
        if (position >= locationList.count()) {
            return LocationSearchViewHolder(View(context))
        }

        val row = locationList[position]
        val inflatedView: View = when (row.type) {
            LocationSearchRowType.HEADER -> parent.inflate(R.layout.recycler_item_location_search_header)
            else -> parent.inflate(R.layout.recycler_item_location_search_result)
        }

        return LocationSearchViewHolder(inflatedView)
    }

    override fun onBindViewHolder(viewHolder: LocationSearchViewHolder, position: Int) {
        val row = locationList[position]

        when(row.type) {
            LocationSearchRowType.HEADER ->
                viewHolder.setHeader(row.header)
            LocationSearchRowType.LOCATION ->
                viewHolder.setLocation(row.location)
        }

        viewHolder.itemView.setOnClickListener {
            listener?.onClickItem(position)
        }
    }

    override fun getItemViewType(position: Int): Int {
        val row = locationList[position]
        return row.type.ordinal
    }

    override fun getItemCount(): Int = locationList.size

    interface LocationSearchRecyclerAdapterListener {
        fun onClickItem(position: Int)
    }
}