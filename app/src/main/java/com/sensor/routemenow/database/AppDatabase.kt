package com.sensor.routemenow.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.google.android.gms.tasks.Task
import com.sensor.routemenow.database.daos.BookmarkLocationDao
import com.sensor.routemenow.database.daos.UserLocationDao
import com.sensor.routemenow.database.entities.BookmarkLocation
import com.sensor.routemenow.database.entities.UserLocation

@Database(entities = [(BookmarkLocation::class), (UserLocation::class)], version = 1, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {

    abstract fun bookmarkLocationDao(): BookmarkLocationDao
    abstract fun userLocationDao(): UserLocationDao

    companion object {
        private var shared: AppDatabase? = null

        fun get(context: Context): AppDatabase {
            if (shared == null) {
                shared = Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "app.db").allowMainThreadQueries().build()
            }
            return shared!!
        }
    }

}