package com.sensor.routemenow.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.sensor.routemenow.database.entities.BookmarkLocation
import com.sensor.routemenow.database.entities.UserLocation

@Dao
interface  BookmarkLocationDao {
    @Query("select * from bookmark_location")
    fun getAll(): LiveData<List<BookmarkLocation>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(bookmarkLocation: BookmarkLocation): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(bookmarkLocation: BookmarkLocation)

    @Delete
    fun delete(bookmarkLocation: BookmarkLocation)

    @Query("DELETE FROM bookmark_location")
    fun deleteAll()
}