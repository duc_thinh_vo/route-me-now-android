package com.sensor.routemenow.database.daos

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.sensor.routemenow.database.entities.UserLocation

@Dao
interface UserLocationDao {

    @Query("select * from user_location")
    fun getAll(): LiveData<List<UserLocation>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(userLocation: UserLocation): Long

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(userLocation: UserLocation)

    @Delete
    fun delete(userLocation: UserLocation)

    @Query("DELETE FROM user_location")
    fun deleteAll()
}