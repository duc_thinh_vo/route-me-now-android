package com.sensor.routemenow.database.entities

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "bookmark_location")
data class BookmarkLocation(
        @PrimaryKey(autoGenerate = true)
        val id: Long = 0,
        @ColumnInfo(name = "longitue")
        val longitue: Double,
        @ColumnInfo(name = "latitude")
        val latitude: Double,
        @ColumnInfo(name = "bookmark_type")
        val bookmarkType: String,
        @ColumnInfo(name = "title")
        val title: String)
