package com.sensor.routemenow.database.entities

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "user_location")
data class UserLocation(
        @PrimaryKey(autoGenerate = true)
        val id: Long = 0,
        val longitue: Double,
        val latitude: Double,
        val label: String)