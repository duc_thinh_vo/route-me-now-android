package com.sensor.routemenow.models

object ItinerariesHolder {
    private var itineraries: MutableList<ItineraryPlanQuery.Itinerary>? = null

    fun set(itineraries: MutableList<ItineraryPlanQuery.Itinerary>?) { ItinerariesHolder.itineraries = itineraries }

    fun get() = itineraries
}