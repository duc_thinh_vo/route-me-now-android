package com.sensor.routemenow.models

enum class LocationBookmarkType {
    WORK,
    SCHOOL,
    SHOPPING,
    RESTAURANT,
    FAVORITE,
    GYM,
    HOME
}