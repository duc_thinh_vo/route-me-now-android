package com.sensor.routemenow.models

enum class LocationSearchRowType {
    HEADER,
    LOCATION
}

data class LocationSearchRow(val type: LocationSearchRowType, val header: String?, val location: Location?) {}