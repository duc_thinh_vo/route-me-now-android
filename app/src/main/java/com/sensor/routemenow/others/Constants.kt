package com.sensor.routemenow.others

object Constants {
    const val CURRENT_LOCATION_UPDATED = "CURRENT_LOCATION_UPATED"
    const val CURRENT_LOCATION_UPDATE_REQUIRED = "CURRENT_LOCATION_UPDATE_REQUIRED"
    const val LONGITUDE = "LONGITUDE"
    const val LATITUDE = "LATITUDE"
    const val LABEL = "LABEL"
}