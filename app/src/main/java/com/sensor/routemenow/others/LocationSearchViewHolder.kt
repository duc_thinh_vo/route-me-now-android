package com.sensor.routemenow.others

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import com.sensor.routemenow.R
import com.sensor.routemenow.models.Location

class LocationSearchViewHolder(val view: View): RecyclerView.ViewHolder(view) {

    // Header
    private var headerTextView: TextView? = null

    // Location search result row
    private var locationName: TextView? = null
    private var locationDescription: TextView? = null

    init {
        when(view.id) {
            R.id.recyler_item_location_search_header ->
                headerTextView = view.findViewById<TextView>(R.id.location_search_header)
            R.id.recyler_item_location_search_result -> {
                locationName = view.findViewById<TextView>(R.id.location_search_name_text_view)
                locationDescription = view.findViewById<TextView>(R.id.location_search_description_text_view)
            }
        }
    }

    fun setHeader(header: String?) {
        headerTextView?.text = header
    }

    fun setLocation(location: Location?) {
        locationName?.text = location?.properties?.name
        locationDescription?.text = location?.properties?.label
    }
}