package com.sensor.routemenow.services

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object GeocodingRetriever {

    private const val URL = "https://api.digitransit.fi/"

    private val retrofit = Retrofit.Builder().baseUrl(URL).addConverterFactory(GsonConverterFactory.create()).build()

    private val geocodingService: GeocodingService = retrofit.create(GeocodingService::class.java)!!

    val service: GeocodingService
     get() = geocodingService
}