package com.sensor.routemenow.services

import com.sensor.routemenow.models.LocationSearchResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GeocodingService {
    @GET("geocoding/v1/search")
    fun searchLocationByName(@Query("text") name: String, @Query("size") numberOfResutls: Int? = null): Call<LocationSearchResult>
}