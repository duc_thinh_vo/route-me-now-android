package com.sensor.routemenow.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.sensor.routemenow.R
import com.sensor.routemenow.database.entities.BookmarkLocation
import com.sensor.routemenow.models.LocationBookmarkType

class BookmarkLocationItemViewModel(val bookmark: BookmarkLocation): ViewModel() {
    val title: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val bookmarkType: MutableLiveData<String> by lazy { MutableLiveData<String>() }

    init {
        title.value = bookmark.title
        bookmarkType.value = bookmark.bookmarkType
    }

    companion object {
        @JvmStatic @BindingAdapter("android:src")
        fun setImage(imageView: ImageView, type: String) {
            val bookmarkType =  LocationBookmarkType.valueOf(type)

            when (bookmarkType) {
                LocationBookmarkType.HOME -> imageView.setImageResource(R.drawable.ic_home)
                LocationBookmarkType.RESTAURANT -> imageView.setImageResource(R.drawable.ic_restaurant)
                LocationBookmarkType.GYM -> imageView.setImageResource(R.drawable.ic_gym)
                LocationBookmarkType.SCHOOL -> imageView.setImageResource(R.drawable.ic_school)
                LocationBookmarkType.WORK -> imageView.setImageResource(R.drawable.ic_work)
                LocationBookmarkType.FAVORITE -> imageView.setImageResource(R.drawable.ic_favorite)
                LocationBookmarkType.SHOPPING -> imageView.setImageResource(R.drawable.ic_shopping)

            }
        }
    }
}