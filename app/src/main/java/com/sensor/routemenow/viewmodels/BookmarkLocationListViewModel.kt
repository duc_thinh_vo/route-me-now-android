package com.sensor.routemenow.viewmodels

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.sensor.routemenow.database.AppDatabase

class BookmarkLocationListViewModel(application: Application): AndroidViewModel(application) {
    private val bookmarkLocations = AppDatabase.get(application).bookmarkLocationDao().getAll()

    fun bookmarkLocations() = bookmarkLocations
}