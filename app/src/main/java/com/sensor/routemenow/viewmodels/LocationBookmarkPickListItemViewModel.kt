package com.sensor.routemenow.viewmodels

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.databinding.BindingAdapter
import android.widget.ImageView
import com.sensor.routemenow.R
import com.sensor.routemenow.models.LocationBookmarkType

class LocationBookmarkPickListItemViewModel(val bookmarkType: LocationBookmarkType): ViewModel() {
    val title: MutableLiveData<String> by lazy { MutableLiveData<String>() }
    val image: MutableLiveData<Int> by lazy { MutableLiveData<Int>() }

    init {
        when(bookmarkType) {
            LocationBookmarkType.HOME -> { title.value = "HOME"; image.value = R.drawable.ic_home }
            LocationBookmarkType.SCHOOL -> { title.value = "SCHOOL"; image.value = R.drawable.ic_school }
            LocationBookmarkType.WORK -> { title.value = "WORK"; image.value = R.drawable.ic_work }
            LocationBookmarkType.GYM -> { title.value = "GYM"; image.value = R.drawable.ic_gym }
            LocationBookmarkType.SHOPPING -> { title.value = "SHOPPING"; image.value = R.drawable.ic_shopping }
            LocationBookmarkType.FAVORITE -> { title.value = "FAVORITE" ; image.value = R.drawable.ic_favorite }
            LocationBookmarkType.RESTAURANT -> { title.value = "RESTAURANT"; image.value = R.drawable.ic_restaurant }
        }
    }

    companion object {
        @JvmStatic @BindingAdapter("android:src")
        fun setImage(imageView: ImageView, imageId: Int) {
            imageView.setImageResource(imageId)
        }
    }
}