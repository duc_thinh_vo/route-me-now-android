package com.sensor.routemenow.views.activities

import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.PolylineOptions
import com.sensor.routemenow.models.ItineraryHolder
import com.sensor.routemenow.R
import com.sensor.routemenow.adapters.LegsRecyclerAdapter
import com.sensor.routemenow.utils.PolylineHelper
import kotlinx.android.synthetic.main.activity_itinerary_detail.*
import type.Mode

class ItineraryDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var map: GoogleMap
    private lateinit var mapView: MapView

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_itinerary_detail)

        mapView = findViewById(R.id.map_itinerary)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)

        val itinerary = ItineraryHolder.get()
        val adapter = LegsRecyclerAdapter(itinerary!!.legs())

        layoutManager = LinearLayoutManager(this)
        itineraryDetailView.layoutManager = layoutManager
        itineraryDetailView.adapter = adapter
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.getUiSettings().setZoomControlsEnabled(true)
        setUpMap()
        drawPolylineOnMap()
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled = true
        map.mapType = GoogleMap.MAP_TYPE_NORMAL
    }

    private fun drawPolylineOnMap() {
        ItineraryHolder.get()?.run {
            val boundsBuilder = LatLngBounds.Builder()
            legs().forEach {
                val polylineOptions = PolylineOptions()

                when(it.mode().toString()) {
                    "BICYCLE" -> { polylineOptions.color(resources.getColor(R.color.colorBike, null))
                    }
                    "BUS"-> { polylineOptions.color(resources.getColor(R.color.colorBus, null))
                    }
                    "FERRY"-> { polylineOptions.color(resources.getColor(R.color.colorFerry, null))
                    }
                    "RAIL"-> { polylineOptions.color(resources.getColor(R.color.colorTrain, null))
                    }
                    "SUBWAY"-> { polylineOptions.color(resources.getColor(R.color.colorMetro, null))
                    }
                    "TRAM" -> { polylineOptions.color(resources.getColor(R.color.colorTram, null))
                    }
                    "WALK" -> { polylineOptions.color(resources.getColor(R.color.colorWalk, null))
                    }
                }
                it.legGeometry()?.points()?.run {
                    val coordinates = PolylineHelper.decode(this)
                    coordinates.forEach {
                        polylineOptions.add(LatLng(it.latitude, it.longitude))
                        boundsBuilder.include(LatLng(it.latitude, it.longitude))
                    }
                }

                map.addPolyline(polylineOptions)
            }

            val width = resources.displayMetrics.widthPixels
            val height = resources.displayMetrics.heightPixels
            val padding = (0.12 * width).toInt()
            val cameraUpate = CameraUpdateFactory.newLatLngBounds(boundsBuilder.build(), width, height, padding)
            map.animateCamera(cameraUpate)
        }
    }
}
