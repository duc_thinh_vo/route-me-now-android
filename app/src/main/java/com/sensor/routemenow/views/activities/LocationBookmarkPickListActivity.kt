package com.sensor.routemenow.views.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.sensor.routemenow.R
import com.sensor.routemenow.adapters.LocationBookmarkPickListAdapter
import com.sensor.routemenow.database.AppDatabase
import com.sensor.routemenow.database.entities.BookmarkLocation
import com.sensor.routemenow.models.LocationBookmarkType
import kotlinx.android.synthetic.main.activity_location_bookmark_pick_list.*

class LocationBookmarkPickListActivity : AppCompatActivity(), LocationBookmarkPickListAdapter.LocationBookmarkPickListAdapterListener {

    companion object {
        private const val LOCATION_SEARTH_REQUEST_CODE = 1
    }

    private var adapter: LocationBookmarkPickListAdapter? = LocationBookmarkPickListAdapter(this)

    private lateinit var layoutManager: RecyclerView.LayoutManager

    private var selectedBookmarkType: LocationBookmarkType? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_location_bookmark_pick_list)

        layoutManager = LinearLayoutManager(this)
        recycler_view_location_bookmark_pick_list.layoutManager = layoutManager
        recycler_view_location_bookmark_pick_list.adapter = adapter
        adapter?.listener = this
        recycler_view_location_bookmark_pick_list.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
    }

    override fun onSelectedLocationBookmarkType(type: LocationBookmarkType) {
        selectedBookmarkType = type

        val intent = Intent(this, LocationSearchActivity::class.java)
        startActivityForResult(intent, LOCATION_SEARTH_REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) { return }

        if (requestCode == LOCATION_SEARTH_REQUEST_CODE) {
            if (selectedBookmarkType != null) {

                data?.extras?.let {
                    val long = it.getDouble(LocationSearchActivity.ACTIVITY_RESULT_LONGITUDE)
                    val lat = it.getDouble(LocationSearchActivity.ACTIVITY_RESULT_LATITUDE)
                    val label = it.getString(LocationSearchActivity.ACTIVITY_RESULT_LABEL)

                    // Save to data base

                    val database = AppDatabase.get(this)
                    val bookmarkLocation = BookmarkLocation(longitue = long, latitude = lat, bookmarkType = selectedBookmarkType!!.toString(),title = label)
                    database.bookmarkLocationDao().insert(bookmarkLocation)
                }

                finish()
            }
        }
    }
}
