package com.sensor.routemenow.views.activities

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import com.google.android.gms.maps.model.LatLng
import com.sensor.routemenow.R
import com.sensor.routemenow.database.AppDatabase
import com.sensor.routemenow.database.entities.UserLocation
import com.sensor.routemenow.views.fragments.LocationSearchFragment
import com.sensor.routemenow.models.Location
import com.sensor.routemenow.others.Constants

class LocationSearchActivity : AppCompatActivity(), LocationSearchFragment.LocationSearchFragmentListener {

    companion object {
        const val ACTIVITY_RESULT_LONGITUDE = "ACTIVITY_RESULT_LONGITUDE"
        const val ACTIVITY_RESULT_LATITUDE = "ACTIVITY_RESULT_LATITUDE"
        const val ACTIVITY_RESULT_LABEL = "ACTIVITY_RESULT_LABEL"
    }

    private lateinit var localBroadcastManager: LocalBroadcastManager
    private var currentLocationCoordinate: LatLng? = null
    private var localBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when (intent?.action) {
                Constants.CURRENT_LOCATION_UPDATED -> {
                    intent.extras.let {
                        val long = it.getDouble(Constants.LONGITUDE)
                        val lat = it.getDouble(Constants.LATITUDE)
                        val label = "${long}, ${lat}"

                        this@LocationSearchActivity.finishSelectingLocation(lat, long, label)
                    }
                }
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        localBroadcastManager = LocalBroadcastManager.getInstance(this)
        localBroadcastManager.registerReceiver(localBroadcastReceiver, IntentFilter(Constants.CURRENT_LOCATION_UPDATED))
        setContentView(R.layout.activity_location_search)
        val fragment = LocationSearchFragment()
        fragment.listener = this

        supportFragmentManager
                .beginTransaction()
                .add(R.id.location_search_container, fragment)
                .commit()
    }

    override fun onLocationSelected(location: Location) {
        val coordinates = location.geometry?.coordinates
        val lon = coordinates?.get(0)
        val lat = coordinates?.get(1)

        val label = location.properties?.label ?: ""
        // Save to history
        if (lon != null && lat != null) {
            val userLocation = UserLocation(longitue = lon, latitude = lat, label = label)
            val userLocationDao = AppDatabase.get(this).userLocationDao()
            userLocationDao.insert(userLocation)
        }

        finishSelectingLocation(lat, lon, label)
    }


    override fun onSelectCurrentLocation() {
        if (currentLocationCoordinate == null) {
            val intent = Intent(Constants.CURRENT_LOCATION_UPDATE_REQUIRED)
            localBroadcastManager.sendBroadcast(intent)
        }
    }

    override fun onSelectLocationOnMap() {

    }

    private fun finishSelectingLocation(lat: Double?, long: Double?, label: String?) {
        val intent = Intent()
        intent.putExtra(ACTIVITY_RESULT_LONGITUDE, long)
        intent.putExtra(ACTIVITY_RESULT_LATITUDE, lat)
        intent.putExtra(ACTIVITY_RESULT_LABEL, label)

        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
