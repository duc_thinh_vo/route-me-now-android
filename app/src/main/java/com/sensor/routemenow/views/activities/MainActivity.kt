package com.sensor.routemenow.views.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.sensor.routemenow.R
import com.sensor.routemenow.database.AppDatabase
import com.sensor.routemenow.views.fragments.LocationBookmarksFragment
import com.sensor.routemenow.views.fragments.MapsFragment
import com.sensor.routemenow.views.fragments.RouteFragment
import com.sensor.routemenow.others.FragmentStateHelper
import com.sensor.routemenow.views.fragments.SettingsFragment
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(),
        MapsFragment.MapsFragmentListener,
        RouteFragment.RouteFragmentListener {

    companion object {
        private const val LOCATION_SEARCH_REQUEST_CODE = 1
        private const val STATE_SAVE_STATE = "save_state"
        private const val STATE_KEEP_FRAGS = "keep_frags"
        private const val STATE_HELPER = "helper"
    }
    private lateinit var stateHelper: FragmentStateHelper

    private val fragments = mutableMapOf<Int, Fragment>()

    private var mapsFragment = MapsFragment()

    private var routeFragment = RouteFragment()

    private var bookmarksFragment = LocationBookmarksFragment()

    private var settingsFragment = SettingsFragment()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_maps -> {
                loadFragment(mapsFragment, item)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_route -> {
                loadFragment(routeFragment, item)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_bookmarks -> {
                loadFragment(bookmarksFragment, item)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                loadFragment(settingsFragment, item)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    private fun saveCurrentState() {
        fragments[navigationMenu.selectedItemId]?.let { oldFragment->
            stateHelper.saveState(oldFragment, navigationMenu.selectedItemId.toString())
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        stateHelper = FragmentStateHelper(supportFragmentManager)

        navigationMenu.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        if (savedInstanceState == null) {
            navigationMenu.selectedItemId = R.id.navigation_maps
        } else {
            val helperState = savedInstanceState.getBundle(STATE_HELPER)
            stateHelper.restoreHelperState(helperState!!)
        }

        routeFragment.listener = this
        mapsFragment.listener = this

        val database = AppDatabase.get(this)
    }

    override fun onSaveInstanceState(outState: Bundle, outPersistentState: PersistableBundle?) {
        outState.putBundle(STATE_HELPER, stateHelper.saveHelperState())
        super.onSaveInstanceState(outState, outPersistentState)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode != Activity.RESULT_OK) {
            return
        }

        when (requestCode) {
            LOCATION_SEARCH_REQUEST_CODE ->
                data?.extras?.let {
                    val long = it.getDouble(LocationSearchActivity.ACTIVITY_RESULT_LONGITUDE)
                    val lat = it.getDouble(LocationSearchActivity.ACTIVITY_RESULT_LATITUDE)
                    val label = it.getString(LocationSearchActivity.ACTIVITY_RESULT_LABEL)

                    when (this@MainActivity.navigationMenu.selectedItemId) {
                        R.id.navigation_route -> routeFragment.onSelectLocation(long, lat, label)
                        R.id.navigation_maps -> mapsFragment.onSelectLocation(long, lat, label)
                    }
                }
        }
    }

    private fun loadFragment(fragment: Fragment, item: MenuItem) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.fragment, fragment)
                .commit()
        if (navigationMenu.selectedItemId != 0) {
            saveCurrentState()
            stateHelper.restoreState(routeFragment, item.itemId.toString())
        }

    }

    /**
     * RouteFragment.RouteFragmentListener methods
     */
    override fun onClickTextView(searchView: View) {
        val intent = Intent(this, LocationSearchActivity::class.java)
        startActivityForResult(intent, LOCATION_SEARCH_REQUEST_CODE)
    }

    override fun onClickSearchView() {
        val intent = Intent(this, LocationSearchActivity::class.java)
        startActivityForResult(intent, LOCATION_SEARCH_REQUEST_CODE)
    }
}
