package com.sensor.routemenow.views.activities

import ItineraryPlanQuery
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.ApolloClient
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import com.sensor.routemenow.R
import com.sensor.routemenow.utils.PolylineHelper
import kotlinx.android.synthetic.main.activity_maps.*
import okhttp3.OkHttpClient
import java.io.IOException

class MapsActivity : AppCompatActivity(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {
    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val LOCATION_SEARCH_REQUEST_CODE = 2
        private const val REQUEST_CHECK_SETTINGS = 3
    }

    private lateinit var locationCallback: LocationCallback

    private lateinit var locationRequest: LocationRequest

    private lateinit var map: GoogleMap

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var locationUpdateState = false

    private lateinit var okHttpClient: OkHttpClient

    private lateinit var apolloClient: ApolloClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        createLocationRequest()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                val location = LatLng(p0.lastLocation.latitude, p0.lastLocation.longitude)
                placeMarkerOnMap(LatLng(p0.lastLocation.latitude, p0.lastLocation.longitude))
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12.0f))
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        okHttpClient = OkHttpClient.Builder().build()

        apolloClient = ApolloClient.builder().serverUrl("https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql").okHttpClient(okHttpClient).build()

        val itineraryQuery = ItineraryPlanQuery.builder().fromLat(60.34770).fromLong(24.86569).toLat(60.16870).toLong(24.93129).build()
        apolloClient.query(itineraryQuery).enqueue(object: ApolloCall.Callback<ItineraryPlanQuery.Data>() {
            override fun onFailure(e: ApolloException) {
                Log.d("ApolloGraphQL", "Failed to ItineraryPlanQuery")
            }

            override fun onResponse(response: Response<ItineraryPlanQuery.Data>) {
                Log.d("ApolloGraphQL", "Success")
                response.data()?.plan()?.itineraries()?.last()?.legs()?.last()?.let {

                    var polylineOptions = PolylineOptions()
                    it.legGeometry()?.points()?.let {
                        val cordinatesList = PolylineHelper.decode(it)
                        cordinatesList.forEach {
                            polylineOptions.add(LatLng(it.latitude, it.longitude))
                        }
                    }

                    this@MapsActivity.runOnUiThread {
                        map.addPolyline(polylineOptions)
                    }
                }
            }
        })

        root_search_view.setOnClickListener { displaySearchActivity() }
        root_search_view.setOnSearchClickListener { displaySearchActivity() }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.getUiSettings().setZoomControlsEnabled(true)
        map.setOnMarkerClickListener(this)

        setUpMap()
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-23.684, 133.903), 4.0f))
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled = true
        map.mapType = GoogleMap.MAP_TYPE_TERRAIN

        fusedLocationClient.lastLocation.addOnSuccessListener(this) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                //lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
                placeMarkerOnMap(currentLatLng)
            }
        }
    }

    private fun placeMarkerOnMap(location: LatLng, description: String? = null) {
        val titleStr = description ?: ""
        val markerOptions = MarkerOptions().position(location)
        //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                //BitmapFactory.decodeResource(resources, R.mipmap.ic_user_location)))
        markerOptions.title(titleStr)
        map.addMarker(markerOptions)
    }

    private fun getAddress(latLng: LatLng): String {
        val geocoder = Geocoder(this)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return addressText
    }

    private fun createLocationRequest() {

        locationRequest = LocationRequest()

        locationRequest.interval = 10000

        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(this)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }

        task.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(this@MapsActivity,
                            REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    private fun  startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CHECK_SETTINGS) {

        }

        when(requestCode) {
            REQUEST_CHECK_SETTINGS -> {
                if (resultCode == Activity.RESULT_OK) {
                    locationUpdateState = true
                    startLocationUpdates()
                }
            }

            LOCATION_SEARCH_REQUEST_CODE ->
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        val lon = it.extras?.get(LocationSearchActivity.ACTIVITY_RESULT_LONGITUDE) as? Double
                        val lat = it.extras?.get(LocationSearchActivity.ACTIVITY_RESULT_LONGITUDE) as? Double
                        if (lon != null && lat != null) {
                            val latlong = LatLng(lat, lon)
                            placeMarkerOnMap(latlong)
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlong, 12.0f))
                        }
                    }
                }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    public override fun onResume() {
        super.onResume()
        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.size == 1 && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                println("Location Permission Granted")
                startLocationUpdates()
            } else {
                // Display error that the location permission is denied or cancelled
                println("Location Permission Denied or Cancelled")
            }
        }
    }

    private fun displaySearchActivity() {
        // Cancel editing immediately
        root_search_view.onActionViewCollapsed()

        val intent = Intent(this, LocationSearchActivity::class.java)
        startActivityForResult(intent, LOCATION_SEARCH_REQUEST_CODE)
    }
}
