package com.sensor.routemenow.views.dialogs

import android.content.pm.ActivityInfo
import com.sensor.routemenow.views.fragments.TimePickerFragment
import android.support.v4.view.ViewPager
import android.support.design.widget.TabLayout
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.sensor.routemenow.R
import com.sensor.routemenow.adapters.SetTimeFragmentPagerAdapter
import kotlinx.android.synthetic.main.dialog_time_picker.*

class TimePickerDialog : DialogFragment(), TimePickerFragment.SetTimeFragmentListener {
    var listener: SetTimeDialogListener? = null
    var done: Boolean = false

    private lateinit var departureTimePickerFragment: TimePickerFragment
    private lateinit var arrivalTimePickerFragment: TimePickerFragment


    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        departureTimePickerFragment = TimePickerFragment()
        arrivalTimePickerFragment = TimePickerFragment()

        departureTimePickerFragment.listener = this
        arrivalTimePickerFragment.listener = this

        val view = inflater.inflate(R.layout.dialog_time_picker, container, false)
        val tabLayout = view.findViewById(R.id.tabLayout) as TabLayout
        val viewPager = view.findViewById(R.id.masterViewPager) as ViewPager
        val adapter = SetTimeFragmentPagerAdapter(childFragmentManager)
        adapter.addFragment("DEPART AT", departureTimePickerFragment)
        adapter.addFragment("ARRIVE BY", arrivalTimePickerFragment)
        viewPager.adapter = adapter
        tabLayout.setupWithViewPager(viewPager)

        return view
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    override fun onSelectTimeListener(hourOfDay: Int, minute: Int) {
        when (masterViewPager.currentItem) {
            0 -> listener?.onSelectTime(hourOfDay, minute, false)
            1 -> listener?.onSelectTime(hourOfDay, minute, true)
        }
        dismiss()
    }

    override fun onCancel() {
        dismiss()
    }

    interface SetTimeDialogListener {
        fun onSelectTime(hourOfDay: Int, minute: Int, arriveBy: Boolean)
    }
}