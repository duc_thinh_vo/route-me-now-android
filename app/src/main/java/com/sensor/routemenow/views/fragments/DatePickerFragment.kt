package com.sensor.routemenow.views.fragments

import android.app.DatePickerDialog
import android.app.Dialog
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.widget.DatePicker
import java.util.*

class DatePickerFragment : DialogFragment(), DatePickerDialog.OnDateSetListener {

    var listener: DatePickerFragmentListener? = null

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(context!!, this, year, month, day)
        dpd.datePicker.minDate = c.timeInMillis
        return dpd
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
        listener?.onDateSet(year, month + 1, dayOfMonth)
    }

    interface DatePickerFragmentListener {
        fun onDateSet(year: Int, month: Int, dayOfMonth: Int)
    }
}