package com.sensor.routemenow.views.fragments

import android.arch.lifecycle.Observer
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sensor.routemenow.R
import com.sensor.routemenow.views.activities.LocationBookmarkPickListActivity
import com.sensor.routemenow.viewmodels.BookmarkLocationListViewModel
import kotlinx.android.synthetic.main.fragment_location_bookmarks.*
import android.arch.lifecycle.ViewModelProviders
import android.content.pm.ActivityInfo
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.sensor.routemenow.adapters.BookmarkLocationRecyclerAdapter

class LocationBookmarksFragment : Fragment() {

    private var bookmarkLocationListViewModel: BookmarkLocationListViewModel? = null
    private lateinit var layoutManager: RecyclerView.LayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_location_bookmarks, container, false)
        bookmarkLocationListViewModel = ViewModelProviders.of(this.activity!!).get(BookmarkLocationListViewModel::class.java)
        bookmarkLocationListViewModel?.bookmarkLocations()?.observe(this, Observer {
            val adapter = BookmarkLocationRecyclerAdapter(this.activity!!, it)
            recyler_view_bookmark_location.adapter = adapter
        })
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        layoutManager = LinearLayoutManager(this.context)

        recyler_view_bookmark_location.layoutManager = layoutManager
        recyler_view_bookmark_location.addItemDecoration(DividerItemDecoration(this.activity, LinearLayoutManager.VERTICAL))
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        fab.setOnClickListener {
            val intent = Intent(this.context, LocationBookmarkPickListActivity::class.java)
            context!!.startActivity(intent)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
    }
}