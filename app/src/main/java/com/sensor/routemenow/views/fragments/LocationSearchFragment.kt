package com.sensor.routemenow.views.fragments

import android.arch.lifecycle.Observer
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import com.sensor.routemenow.R
import com.sensor.routemenow.adapters.LocationSearchRecyclerAdapter
import com.sensor.routemenow.database.AppDatabase
import com.sensor.routemenow.models.*
import com.sensor.routemenow.services.GeocodingRetriever
import kotlinx.android.synthetic.main.fragment_location_search.*
import retrofit2.Call
import retrofit2.Callback

class LocationSearchFragment : Fragment(), SearchView.OnQueryTextListener, LocationSearchRecyclerAdapter.LocationSearchRecyclerAdapterListener {

    var listener: LocationSearchFragmentListener? = null

    private var adapter: LocationSearchRecyclerAdapter? = null

    private lateinit var linearLayoutManager: LinearLayoutManager

    private var  rows: List<LocationSearchRow>? = null

    private var searching = false

    private var searchingText: String? =  null

    private lateinit var database: AppDatabase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        linearLayoutManager = LinearLayoutManager(context)
        database = AppDatabase.get(this.context!!)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_location_search, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        location_search_view.setOnQueryTextListener(this)
        button_select_current_location.setOnClickListener {
            listener?.onSelectCurrentLocation()
        }

        button_select_location_on_map.setOnClickListener {
            listener?.onSelectLocationOnMap()
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        location_search_view.onActionViewExpanded()
        location_search_recycler_view.layoutManager = linearLayoutManager
        location_search_recycler_view.adapter = adapter
    }

    override fun onQueryTextSubmit(query: String?): Boolean = true

    override fun onQueryTextChange(newText: String?): Boolean {
        searchingText = newText
        if (newText == null) {
            searching = false
        } else {
            searching = !newText.equals("")
        }

        if (searching) {
            // Make network query
            val geocodingCall = GeocodingRetriever.service.searchLocationByName(newText!!)
            geocodingCall.enqueue(object : Callback<LocationSearchResult> {
                override fun onFailure(call: Call<LocationSearchResult>, t: Throwable) {
                    location_search_recycler_view.adapter = null
                }

                override fun onResponse(call: Call<LocationSearchResult>, response: retrofit2.Response<LocationSearchResult>) {
                    val locations = response.body()?.features
                    val locationSearchResults = locations?.map {
                        LocationSearchRow(LocationSearchRowType.LOCATION, null, it)
                    }

                    rows = locationSearchResults

                    locationSearchResults?.let {
                        if (context != null) {
                            adapter = LocationSearchRecyclerAdapter(this@LocationSearchFragment.context!!, it)
                            adapter?.listener = this@LocationSearchFragment
                            location_search_recycler_view?.adapter = adapter
                        }
                    }
                }
            })
        } else {
            // Display recent search results
            database.userLocationDao().getAll().observe(this, Observer {
                if (!searching) {
                    val historyLocationList = mutableListOf<LocationSearchRow>()
                    val historyHeader = LocationSearchRow(LocationSearchRowType.HEADER, "Recently search", null)
                    historyLocationList.add(historyHeader)
                    it?.forEach {
                        val geometry = Geometry("", listOf(it.longitue, it.latitude))
                        val properties = LocationProperties("", "","", "", "", "", "", "", null, "", it.label)
                        var location = Location(geometry = geometry, properties = properties)
                        val row = LocationSearchRow(LocationSearchRowType.LOCATION, null, location)
                        historyLocationList.add(row)
                    }
                    rows = historyLocationList
                    adapter = rows?.let { LocationSearchRecyclerAdapter(this.context!!, it) }
                    adapter?.listener = this
                    location_search_recycler_view.adapter = null
                    location_search_recycler_view.adapter = adapter
                }
            })
        }

        return true
    }

    override fun onClickItem(position: Int) {
        rows?.get(position)?.location?.let {
            listener?.onLocationSelected(it)
        }
    }

    interface LocationSearchFragmentListener {
        fun onLocationSelected(location: Location)
        fun onSelectCurrentLocation()
        fun onSelectLocationOnMap()
    }
}
