package com.sensor.routemenow.views.fragments

import android.app.Activity
import android.content.*
import android.content.pm.ActivityInfo
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.apollographql.apollo.ApolloClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sensor.routemenow.R
import com.sensor.routemenow.others.Constants
import kotlinx.android.synthetic.main.fragment_maps.*
import okhttp3.OkHttpClient
import java.io.IOException

class MapsFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private lateinit var locationCallback: LocationCallback

    private lateinit var locationRequest: LocationRequest

    private lateinit var map: GoogleMap

    private lateinit var mapView: MapView

    private lateinit var fusedLocationClient: FusedLocationProviderClient

    private var locationUpdateState = false

    private lateinit var okHttpClient: OkHttpClient

    private lateinit var apolloClient: ApolloClient

    var listener: MapsFragment.MapsFragmentListener? = null
    private var currentLocationCoordiate: LatLng? = null

    private lateinit var localBroadcastManager: LocalBroadcastManager
    private val localBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            when(intent?.action) {
                Constants.CURRENT_LOCATION_UPDATE_REQUIRED -> {
                    val intent = Intent(Constants.CURRENT_LOCATION_UPDATED)
                    intent.putExtra(Constants.LONGITUDE, currentLocationCoordiate?.longitude)
                    intent.putExtra(Constants.LATITUDE, currentLocationCoordiate?.latitude)
                    localBroadcastManager.sendBroadcast(intent)
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        mapView = view.findViewById(R.id.map)
        mapView.onCreate(savedInstanceState)
        mapView.onResume()
        mapView.getMapAsync(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        localBroadcastManager = LocalBroadcastManager.getInstance(this.context!!)
        localBroadcastManager.registerReceiver(localBroadcastReceiver, IntentFilter(Constants.CURRENT_LOCATION_UPDATE_REQUIRED))

        createLocationRequest()
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)

                val location = LatLng(p0.lastLocation.latitude, p0.lastLocation.longitude)
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 16.0f))

                currentLocationCoordiate = location

                // Broadcast the location data
                val intent = Intent(Constants.CURRENT_LOCATION_UPDATED)
                intent.putExtra(Constants.LONGITUDE, p0.lastLocation.longitude)
                intent.putExtra(Constants.LATITUDE, p0.lastLocation.latitude)
                localBroadcastManager.sendBroadcast(intent)
            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)

        okHttpClient = OkHttpClient.Builder().build()

        apolloClient = ApolloClient.builder().serverUrl("https://api.digitransit.fi/routing/v1/routers/hsl/index/graphql").okHttpClient(okHttpClient).build()
        locationInput.setOnClickListener { listener?.onClickSearchView() }
    }

    companion object {
        private const val LOCATION_PERMISSION_REQUEST_CODE = 1
        private const val LOCATION_SEARCH_REQUEST_CODE = 1
        private const val REQUEST_CHECK_SETTINGS = 2
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map.getUiSettings().setZoomControlsEnabled(true)
        map.setOnMarkerClickListener(this)

        setUpMap()
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        return false
    }

    private fun setUpMap() {
        if (ActivityCompat.checkSelfPermission(context!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), MapsFragment.LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        map.isMyLocationEnabled = true
        map.mapType = GoogleMap.MAP_TYPE_NORMAL

        fusedLocationClient.lastLocation.addOnSuccessListener(activity!!) { location ->
            // Got last known location. In some rare situations this can be null.
            if (location != null) {
                //lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                map.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 12f))
            }
        }
    }

    private fun placeMarkerOnMap(location: LatLng, description: String? = null) {
        val titleStr = description ?: ""
        val markerOptions = MarkerOptions().position(location)
        //markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
        //BitmapFactory.decodeResource(resources, R.mipmap.ic_user_location)))
        markerOptions.title(titleStr)
        map.addMarker(markerOptions)
    }

    private fun getAddress(latLng: LatLng): String {
        val geocoder = Geocoder(context)
        val addresses: List<Address>?
        val address: Address?
        var addressText = ""

        try {
            addresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
            if (null != addresses && !addresses.isEmpty()) {
                address = addresses[0]
                for (i in 0 until address.maxAddressLineIndex) {
                    addressText += if (i == 0) address.getAddressLine(i) else "\n" + address.getAddressLine(i)
                }
            }
        } catch (e: IOException) {
            Log.e("MapsActivity", e.localizedMessage)
        }

        return addressText
    }

    private fun createLocationRequest() {

        locationRequest = LocationRequest()

        locationRequest.interval = 10000

        locationRequest.fastestInterval = 5000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(activity!!)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            startLocationUpdates()
        }

        task.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(activity,
                            MapsFragment.REQUEST_CHECK_SETTINGS)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    private fun  startLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(context!!,
                        android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!,
                    arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION),
                    MapsFragment.LOCATION_PERMISSION_REQUEST_CODE)
            return
        }

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == MapsFragment.REQUEST_CHECK_SETTINGS) {

        }

        when(requestCode) {
            MapsFragment.REQUEST_CHECK_SETTINGS -> {
                if (resultCode == Activity.RESULT_OK) {
                    locationUpdateState = true
                    startLocationUpdates()
                }
            }

            MapsFragment.LOCATION_SEARCH_REQUEST_CODE ->
                if (resultCode == Activity.RESULT_OK) {
                    data?.let {
                        val lon = it.extras?.get("lon") as? Double
                        val lat = it.extras?.get("lat") as? Double
                        if (lon != null && lat != null) {
                            val latlong = LatLng(lat, lon)
                            placeMarkerOnMap(latlong)
                            map.moveCamera(CameraUpdateFactory.newLatLngZoom(latlong, 12.0f))
                        }
                    }
                }
        }
    }

    override fun onPause() {
        super.onPause()
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        if (!locationUpdateState) {
            startLocationUpdates()
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == MapsFragment.LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.size == 1 && grantResults.first() == PackageManager.PERMISSION_GRANTED) {
                println("Location Permission Granted")
                startLocationUpdates()
            } else {
                // Display error that the location permission is denied or cancelled
                println("Location Permission Denied or Cancelled")
            }
        }
    }

    fun onSelectLocation(long: Double, lat: Double, label: String?) {
        locationInput.text = label
        val location = LatLng(lat, long)
        placeMarkerOnMap(LatLng(lat, long), label)
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 12.0f))
    }

    interface MapsFragmentListener {
        fun onClickSearchView()
    }
}
