package com.sensor.routemenow.views.fragments

import android.content.DialogInterface
import android.content.Intent
import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.apollographql.apollo.ApolloCall
import com.apollographql.apollo.api.Response
import com.apollographql.apollo.exception.ApolloException
import com.sensor.routemenow.models.ItinerariesHolder
import com.sensor.routemenow.models.ItineraryHolder
import com.sensor.routemenow.R
import com.sensor.routemenow.views.activities.ItineraryDetailActivity
import com.sensor.routemenow.adapters.ItineraryResultsRecyclerAdapter
import com.sensor.routemenow.views.dialogs.TimePickerDialog
import com.sensor.routemenow.models.Coordinate
import com.sensor.routemenow.services.Apollo
import kotlinx.android.synthetic.main.fragment_route.*
import java.util.*
import android.app.ProgressDialog

class RouteFragment : Fragment(),
        TimePickerDialog.SetTimeDialogListener,
        ItineraryResultsRecyclerAdapter.ItineraryResultsRecyclerAdapterListener,
        DatePickerFragment.DatePickerFragmentListener {

    companion object {
        private val STATE_FROM_LOCATION: String = ""
        private val STATE_TO_LOCATION: String = ""
        private val FROM_COORDINATE_LAT: Coordinate? = null
        private val FROM_COORDINATE_LONG: Coordinate? = null
        private val TO_COORDINATE: Coordinate? = null
    }

    private var lastSelectedSearchView: View? = null

    var listener: RouteFragmentListener? = null

    var fromCoordinate: Coordinate? = null
    var toCoordinate: Coordinate? = null


    var calendar = GregorianCalendar()

    var hourOfDay = calendar.get(Calendar.HOUR_OF_DAY)
    var minute = calendar.get(Calendar.MINUTE)

    var year = calendar.get(Calendar.YEAR)
    var month = calendar.get(Calendar.MONTH) + 1
    var dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH)
    var arriveBy: Boolean = false

    private lateinit var layoutManager: LinearLayoutManager

    private var itineraries: MutableList<ItineraryPlanQuery.Itinerary>? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_route, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonSetTime.text = "Depart at $hourOfDay:$minute"
        buttonSetDate.text = "$dayOfMonth-$month-$year"
        setupTextView(fromLocationInput)
        setupTextView(toLocationInput)

        if (savedInstanceState != null) {
            fromLocationInput.text = savedInstanceState.getString(STATE_FROM_LOCATION)
            toLocationInput.text = savedInstanceState.getString(STATE_TO_LOCATION)
            queryItineraryPlan()
            Log.d("DBG", savedInstanceState.toString())
        }

        buttonSwap.setOnClickListener {
            fromLocationInput.text = toLocationInput.text.also { toLocationInput.text = fromLocationInput.text }
            fromCoordinate = toCoordinate.also { toCoordinate = fromCoordinate }
            queryItineraryPlan()
        }

        buttonSelectOptions.setOnClickListener {
            buttonSelectOptionsOnClick()
        }

        buttonSetTime.setOnClickListener {
            showTimePickerDialog()
        }

        buttonSetDate.setOnClickListener {
            val newFragment = DatePickerFragment()
            newFragment.listener = this
            newFragment.show(childFragmentManager, "datePicker")
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(STATE_FROM_LOCATION, fromLocationInput.text.toString())
        outState.putString(STATE_TO_LOCATION, toLocationInput.text.toString())
        ItinerariesHolder.set(itineraries)
    }

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)

        if (savedInstanceState != null)
            itineraries = ItinerariesHolder.get()!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        layoutManager = LinearLayoutManager(context)
    }

    override fun onResume() {
        super.onResume()
        itineraryResultView.layoutManager = layoutManager
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

    }

    private fun setupTextView(textView: TextView) {
        textView.setOnClickListener {
            lastSelectedSearchView = it
            listener?.onClickTextView(it)
        }
    }

    fun showTimePickerDialog() {
        val setTimeDialog = TimePickerDialog()
        setTimeDialog.listener = this
        setTimeDialog.show(childFragmentManager, "dialog")
    }

    private fun buttonSelectOptionsOnClick() {
        val builder: AlertDialog.Builder = activity!!.let {
            AlertDialog.Builder(it)
        }
        val dialogView = layoutInflater.inflate(R.layout.dialog_itinerary_options, null)

        builder.setView(dialogView)
        builder.setTitle("Options")
        builder.apply {
            setPositiveButton("DONE",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
            setNegativeButton("CANCEL",
                    DialogInterface.OnClickListener { dialog, id ->
                    })
        }
        builder.create().show()
    }

    fun onSelectLocation(long: Double, lat: Double, label: String?) {
        val coordinate = Coordinate(long, lat)

        lastSelectedSearchView?.let {
            if (it === fromLocationInput) {
                fromCoordinate = coordinate
            } else {
                toCoordinate = coordinate
            }
        }

        (lastSelectedSearchView as? TextView)?.run {
            text = label
        }
        queryItineraryPlan()
    }

    fun queryItineraryPlan() {
        if (fromCoordinate != null && toCoordinate != null) {
            val mDialog = ProgressDialog(context)
            mDialog.setProgressStyle(0)
            mDialog.setCancelable(false)
            mDialog.setMessage("Loading...")
            mDialog.show()
            Apollo.apolloClient.query(ItineraryPlanQuery
                    .builder()
                    .fromLat(fromCoordinate!!.latitude)
                    .fromLong(fromCoordinate!!.longitude)
                    .toLat(toCoordinate!!.latitude)
                    .toLong(toCoordinate!!.longitude)
                    .date("$year-$month-$dayOfMonth")
                    .time("$hourOfDay:$minute")
                    .arriveBy(arriveBy)
                    .build()
            ).enqueue(object : ApolloCall.Callback<ItineraryPlanQuery.Data>() {
                override fun onFailure(e: ApolloException) {
                    Log.d("DBG", "Cannot get data")
                    errorMessageTextView.visibility = View.VISIBLE
                    errorMessageTextView.text = "Cannot get route"
                    mDialog.dismiss()
                }

                override fun onResponse(response: Response<ItineraryPlanQuery.Data>) {
                    itineraries = response.data()?.plan()?.itineraries()

                    if (itineraries?.size != 0) {
                        this@RouteFragment.activity?.runOnUiThread {
                            errorMessageTextView.visibility = View.GONE
                            itineraryResultView.visibility = View.VISIBLE
                            val adapter = ItineraryResultsRecyclerAdapter(itineraries!!)
                            adapter.listener = this@RouteFragment
                            itineraryResultView.adapter = adapter
                            mDialog.dismiss()
                        }

                    } else {
                        this@RouteFragment.activity?.runOnUiThread {
                            errorMessageTextView.visibility = View.VISIBLE
                            itineraryResultView.visibility = View.GONE
                            errorMessageTextView.text = "Cannot get route"
                            mDialog.dismiss()
                        }
                    }
                }
            })
        }
    }

    override fun onSelectTime(hourOfDay: Int, minute: Int, arriveBy: Boolean) {
        this.hourOfDay = hourOfDay
        this.minute = minute
        this.arriveBy = arriveBy
        if (arriveBy == false) {
            buttonSetTime.text = "Depart at $hourOfDay:$minute "
        } else {
            buttonSetTime.text = "Arrive by $hourOfDay:$minute"
        }
        queryItineraryPlan()
    }

    override fun onClickItineraryItem(position: Int) {
        val itinerary = itineraries!![position]
        val intent = Intent(activity, ItineraryDetailActivity::class.java)
        ItineraryHolder.set(itinerary)
        startActivity(intent)
    }

    override fun onDateSet(year: Int, month: Int, dayOfMonth: Int) {
        buttonSetDate.text = "$dayOfMonth-$month-$year "
        this.year = year
        this.month = month
        this.dayOfMonth = dayOfMonth
        queryItineraryPlan()
    }


    interface RouteFragmentListener {
        fun onClickTextView(searchView: View)
    }
}
