package com.sensor.routemenow.views.fragments

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.sensor.routemenow.R
import com.sensor.routemenow.database.AppDatabase
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment : Fragment() {

    private lateinit var database: AppDatabase

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_settings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        settings_toolbar.setTitle(R.string.title_fragment_setting)

        button_remove_history.setOnClickListener {
            val message = resources.getString(R.string.remove_location_search_history_confirmation_message)
            displayAlertForMessage(message,
                    {
                        database.userLocationDao().deleteAll()
                        displayToast()
                    },
                    {
                        displayToast(resources.getString(R.string.cancelled))
                    })
        }

        button_remove_bookmark.setOnClickListener {
            val message = resources.getString(R.string.remove_location_bookmark_confirmation_message)
            displayAlertForMessage(message,
                    {
                        database.bookmarkLocationDao().deleteAll()
                        displayToast()
                    },
                    {
                        displayToast(resources.getString(R.string.cancelled))
                    })
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        database = AppDatabase.get(this.context!!)
    }

    private fun displayAlertForMessage(message: String, yesAction: () -> Unit, noAction: () -> Unit) {
        val buidler = AlertDialog.Builder(this.context!!)
        val yesString = resources.getString(R.string.yes)
        val noString = resources.getString(R.string.no)
        buidler.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(yesString, object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        yesAction()
                    }
                })
                .setNegativeButton(noString, object : DialogInterface.OnClickListener {
                    override fun onClick(dialog: DialogInterface?, which: Int) {
                        noAction()
                    }
                })
        val dialog = buidler.create()
        dialog.show()
    }

    private fun displayToast(message: String = resources.getString(R.string.deleted)) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_LONG).show();
    }
}
