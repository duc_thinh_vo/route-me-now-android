package com.sensor.routemenow.views.fragments

import android.content.pm.ActivityInfo
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import com.sensor.routemenow.R
import kotlinx.android.synthetic.main.fragment_time_picker.*
import java.util.*

class TimePickerFragment : Fragment() {

    val calendar = GregorianCalendar()
    var hourOfDay = calendar.get(Calendar.HOUR_OF_DAY)
    var minute = calendar.get(Calendar.MINUTE)

    var listener: SetTimeFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_time_picker, container, false)
        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        timePicker.setIs24HourView(true)

        timePicker.setOnTimeChangedListener { view, hourOfDay, minute ->
            this.hourOfDay = hourOfDay
            this.minute = minute
        }

        buttonCancelSetTime.setOnClickListener {
            listener?.onCancel()
        }

        buttonSetNewTime.setOnClickListener {
            listener?.onSelectTimeListener(hourOfDay, minute)
        }
    }

    override fun onResume() {
        super.onResume()
        activity?.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    interface SetTimeFragmentListener {
        fun onSelectTimeListener(hourOfDay: Int, minute: Int)
        fun onCancel()
    }
}